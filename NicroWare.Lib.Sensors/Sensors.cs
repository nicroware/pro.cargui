﻿using System;
using System.Collections.Generic;

namespace NicroWare.Lib.Sensors
{
    public class SensorLookup
    {
        static List<SensorLookup> allLoockUps = new List<SensorLookup>();
        static List<ushort> sensorIDs = new List<ushort>();
        public static ushort[] SensorIDs 
        {
            get
            { 
                return sensorIDs.ToArray();
            }
        }

        public const string SPEED = "SPEED";

        public const string SOC = "SOC";
        public const string CURRENT = "CURRENT";
        public const string VOLTAGE = "VOLTAGE";
        public const string VOLTAGE12 = "VOLTAGE12";



        public const string TEMPBAT = "TEMPBAT";
        public const string TEMPCOOL = "TEMPCOOL";

        public const string DRVWHEEL = "DRVWHEEL";
        public const string SPEEDPEDDAL = "SPEEDPEDDAL";
        public const string BREAKPEDDAL = "BREAKPEDDAL";
        public const string SUSPENSION = "SUSPENSION";

        public const string GPSDEG = "GPSDEG";
        public const string GPSMIN = "GPSMIN";
        public const string LAT = "LAT";
        public const string LONG = "LONG";


        public const string PDLERR = "PDLERR";
        public const string STUPERR = "STUPERR";
        public const string MRC3ERR = "MRC3ERR";
        public const string MRC4ERR = "MRC4ERR";

        //public const string GYRO = "GYRO";
        //public const string ACCEL = "ACCEL";

        //public const string RPM = "RPM";
        //public const string POS = "POS";

        static SensorLookup()
        {
            allLoockUps.Add(new SensorLookup(0, SPEED, "Speed", 0, 0xFFF, 1, 0, 300));

            allLoockUps.Add(new SensorLookup(1, SOC, "State Of Charge", 0, 0xFFF, 0.2, 0, 100));
            allLoockUps.Add(new SensorLookup(2, VOLTAGE, "Voltage", 0, 0xFFF, 0.2, 0, 1));
            allLoockUps.Add(new SensorLookup(3, CURRENT, "Current", 0, 0xFFF, 0.2, 0, 1));
            allLoockUps.Add(new SensorLookup(4, VOLTAGE12, "Voltage 12V battery", 3500, 4000, 0.2, 12, 14));

            allLoockUps.Add(new SensorLookup(5, TEMPBAT, "Battery Temperature", 0, 0xFFF, 0.2, 20, 80));
            allLoockUps.Add(new SensorLookup(6, TEMPCOOL, "Coolant Temperature", 0, 0xFFF, 0.2, 20, 80));

            allLoockUps.Add(new SensorLookup(7, DRVWHEEL, "Drive Wheel", 0, 0xFFF, 0));
            allLoockUps.Add(new SensorLookup(8, SPEEDPEDDAL, "Speed Peddal", 0, 0xFFF, 0));
            allLoockUps.Add(new SensorLookup(9, BREAKPEDDAL, "Break Peddal", 0, 0xFFF, 0));
            allLoockUps.Add(new SensorLookup(10, SUSPENSION, "Suspension", 0, 0xFFF, 0));

            allLoockUps.Add(new SensorLookup(200, GPSDEG, "Degree", 0, ushort.MaxValue, 0));
            allLoockUps.Add(new SensorLookup(201, GPSMIN, "Minute", 0, ushort.MaxValue, 0));
            allLoockUps.Add(new SensorLookup(202, LAT, "Latitude", 0, ushort.MaxValue, 0.2));
            allLoockUps.Add(new SensorLookup(203, LONG, "Longitude", 0, ushort.MaxValue, 0.2));


            allLoockUps.Add(new SensorLookup(100, PDLERR, "Peddal Error"));
            allLoockUps.Add(new SensorLookup(101, STUPERR, "Startup Error"));
            allLoockUps.Add(new SensorLookup(102, MRC3ERR, "Micro 3 Error"));
            allLoockUps.Add(new SensorLookup(103, MRC4ERR, "Micro 4 Error"));

            allLoockUps.ForEach(x => sensorIDs.Add(x.ID));
        }

        public static SensorLookup GetById(ushort id)
        {
            return allLoockUps.Find(x => x.ID == id);
        }

        public static SensorLookup GetByName(string name)
        {
            return allLoockUps.Find(x => x.Name.ToUpper() == name.ToUpper());
        }

        public SensorLookup(ushort id, string name)
            :this(id, name, null)
        {
        }

        public SensorLookup(ushort id, string name, string displayName)
            :this(id, name, displayName, -1, -1)
        {
            
        }

        public SensorLookup(ushort id, string name, string displayName, int lowLimit, int highLimit)
            :this(id, name, displayName, lowLimit, highLimit, 1)
        {
            
        }

        public SensorLookup(ushort id, string name, string displayName, int lowLimit, int highLimit, double importance)
        {
            this.ID = id;
            this.Name = name;
            this.displayName = displayName;
            this.LowLimit = lowLimit;
            this.HighLimit = highLimit;
            this.Importance = importance;
        }

        public SensorLookup(ushort id, string name, string displayName, int lowLimit, int highLimit, double importance, double minValue, double maxValue)
            :this(id, name, displayName, lowLimit, highLimit, importance)
        {
            this.MinValue = minValue;
            this.MaxValue = maxValue;
        }


        public double Importance { get; private set; }
        public int LowLimit { get; private set; }
        public int HighLimit { get; private set; }
        /// <summary>
        /// Returns HighLimit - LowLimit
        /// </summary>
        public int SpanLimit
        {
            get
            { 
                return HighLimit - LowLimit;
            }
        }
        public double MaxValue { get; private set; }
        public double MinValue { get; private set; }

        public ushort ID { get; private set; } 
        public string Name { get; private set; }
        string displayName;
        public string DisplayName 
        { 
            get
            { 
                return displayName ?? Name;
            }
            set
            { 
                displayName = value;
            }
        }

        public double ConvertValue(int rawValue)
        {
            return ConvertToPercent(rawValue) * (MaxValue - MinValue) + MinValue;
        }

        public double ConvertToPercent(int rawValue)
        {
            return ((double)(rawValue - LowLimit) / (double)(HighLimit - LowLimit));
        }

        public override bool Equals(object obj)
        {
            if (obj.GetType() == typeof(int))
            {
                return (int)obj == ID;
            }
            else if (obj.GetType() == typeof(SensorLookup))
            {
                return ((SensorLookup)obj).ID == ID;
            }
            else
                return obj.Equals(this);
        }
    }
}

