﻿using System;
using System.Collections.Generic;

namespace NicroWare.Lib.Sensors
{
    public class CanError
    {
        static List<CanError> allErrors = new List<CanError>();
        static bool Init = false;
        public static void Initialize()
        {

            #region Startup errors
            allErrors.Add(new CanError(101, 1, "Startup Error Torque"));
            allErrors.Add(new CanError(101, 2, "Startup Error Motor Read"));
            allErrors.Add(new CanError(101, 3, "Startup Error Left Motor Bad Paras"));
            allErrors.Add(new CanError(101, 4, "Startup Error Left Motor Powerfault"));
            allErrors.Add(new CanError(101, 5, "Startup Error Left Motor Illigal"));
            allErrors.Add(new CanError(101, 6, "Startup Error Left Motor Can timeout"));
            allErrors.Add(new CanError(101, 7, "Startup Error Left Motor Reso signal"));
            allErrors.Add(new CanError(101, 8, "Startup Error Left Motor Power Voltage"));
            allErrors.Add(new CanError(101, 9, "Startup Error Left Motor Motor Temp"));
            allErrors.Add(new CanError(101, 10, "Startup Error Left Motor IDC"));
            allErrors.Add(new CanError(101, 11, "Startup Error Left Motor I 123"));
            allErrors.Add(new CanError(101, 12, "Startup Error Left Motor I Peak"));
            allErrors.Add(new CanError(101, 13, "Startup Error Left Motor Raceaway"));
            allErrors.Add(new CanError(101, 14, "Startup Error Left Motor Can Init"));
            allErrors.Add(new CanError(101, 15, "Startup Error Left Motor SPI ADC Init"));
            allErrors.Add(new CanError(101, 16, "Startup Error Left Motor Rotor"));
            allErrors.Add(new CanError(101, 17, "Startup Error Left Motor ADC TNT"));
            allErrors.Add(new CanError(101, 18, "Startup Error Left Motor Ballast"));
            allErrors.Add(new CanError(101, 19, "Startup Error Right Motor Bad Paras"));
            allErrors.Add(new CanError(101, 20, "Startup Error Right Motor Powerfault"));
            allErrors.Add(new CanError(101, 21, "Startup Error Right Motor Illigal"));
            allErrors.Add(new CanError(101, 22, "Startup Error Right Motor Can timeout"));
            allErrors.Add(new CanError(101, 23, "Startup Error Right Motor Reso signal"));
            allErrors.Add(new CanError(101, 24, "Startup Error Right Motor Power Voltage"));
            allErrors.Add(new CanError(101, 25, "Startup Error Right Motor Motor Temp"));
            allErrors.Add(new CanError(101, 26, "Startup Error Right Motor IDC"));
            allErrors.Add(new CanError(101, 27, "Startup Error Right Motor I 123"));
            allErrors.Add(new CanError(101, 28, "Startup Error Right Motor I Peak"));
            allErrors.Add(new CanError(101, 29, "Startup Error Right Motor Raceaway"));
            allErrors.Add(new CanError(101, 30, "Startup Error Right Motor Can Init"));
            allErrors.Add(new CanError(101, 31, "Startup Error Right Motor SPI ADC Init"));
            allErrors.Add(new CanError(101, 32, "Startup Error Right Motor Rotor"));
            allErrors.Add(new CanError(101, 33, "Startup Error Right Motor ADC TNT"));
            allErrors.Add(new CanError(101, 34, "Startup Error Right Motor Ballast"));
            allErrors.Add(new CanError(101, 35, "Startup Error Motor Ready Left State Ready"));
            allErrors.Add(new CanError(101, 36, "Startup Error Motor Ready Right State Ready"));
            allErrors.Add(new CanError(101, 37, "Startup Error Motor Enabled Left Enabled Off"));
            allErrors.Add(new CanError(101, 38, "Startup Error Motor Enabled Right Enabled Off"));
            #endregion

            Init = true;
        }

        public static CanError GetError(int sensorID, int value)
        {
            if (!Init)
                Initialize();
            foreach (CanError error in allErrors)
            {
                if (error.SensorID == sensorID && error.ValueID == value)
                {
                    return error;
                }
            }
            return new CanError(0, 0, "Unknown error");
        }

        public string Name { get; private set; }

        public int SensorID { get; private set; }
        public int ValueID { get; private set; }

        public string Msg { get; private set; }

        public CanError(int sensorId, int valueId, string msg)
        {
            this.SensorID = sensorId;
            this.ValueID = valueId;
            this.Msg = msg;
        }

        public override string ToString()
        {
            return Msg;
        }
    }
}

