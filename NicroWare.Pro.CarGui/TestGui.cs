﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Linq;
using NicroWare.Lib.SdlEngine;

namespace NicroWare.Pro.SdlEngine
{
    /*public class TestGui : SDLRenderWindow
    {
        //Hello World, this is a comment
        SDLRenderer renderer;
        SDLTexture texture;
        SDLSurface textSurface;
        SDLTexture textTexture;
        SDLTexture tester;

        SDLRectangle rect;
        SDLRectangle rotation;
        SDLRectangle textRect;
        DateTime now;
        DateTime last;

        int c;
        int[] allInts;

        IntPtr ptr;

        public override void Initialize()
        {
            base.Initialize();
            renderer = SDLRenderer.Create(this);
        }

        public override void LoadContent()
        {
            base.LoadContent();

            FileInfo bitmapFile = new FileInfo("Content\\bitmap_image.bmp");
            FileInfo textFile = new FileInfo("Content\\SFDigital.ttf");

            SDLSurface bitmap = SDLSurface.LoadBitmap(bitmapFile.FullName);
            texture = SDLTexture.CreateFrom(renderer, bitmap);

            bitmap.Dispose();

            ptr = SDL_ttf.TTF_OpenFont(textFile.FullName, 150);
            textSurface = SDLSurface.FromPointer(SDL_ttf.TTF_RenderText_Solid(ptr, "Hello World", new SDL.SDL_Color() { r = 255, g = 255, b = 255}));
            textTexture = SDLTexture.CreateFrom(renderer, textSurface);
            rect = new SDLRectangle();
            rotation = new SDLRectangle(0, 0, 0, 0);
            textRect = new SDLRectangle(0, 0, 36 * 3 + 30, 72 + 30);

            SDLTextureQuery info = texture.Query(); 

            rect.Width = info.width;
            rect.Height = info.height;
            rotation.Width = info.width;
            rotation.Height = info.height;


            IntPtr pixels;
            int pitch;
            SDL.SDL_Rect rect2 = new SDL.SDL_Rect()  { w = 100, h = 100 };
            IntPtr test = SDL.SDL_CreateTexture(renderer.BasePointer, SDL.SDL_PIXELFORMAT_RGB24, (int)SDL.SDL_TextureAccess.SDL_TEXTUREACCESS_STREAMING, 100, 100);
            int error = SDL.SDL_LockTexture(test, ref rect2, out pixels, out pitch);
            if (error != 0)
                Console.WriteLine(SDL.SDL_GetError());
            byte[] buffer = new byte[100*100*3];
            Marshal.Copy(pixels, buffer, 0, buffer.Length);
            for (int cor = 0; cor < 100*100*3; cor+= 3)
            {
                buffer[cor] = 255;
            }
            Marshal.Copy(buffer, 0, pixels, buffer.Length);
            SDL.SDL_UnlockTexture(test);
            tester = new SDLTexture(test);


            c = 0;

            now = DateTime.Now;

            allInts = new int[500];
        }

        public void DoEvent()
        {
            SDL.SDL_Event e;
            while (SDL.SDL_PollEvent(out e) != 0)
            {
                if (e.type == SDL.SDL_EventType.SDL_QUIT)
                    Quit();
                if (e.type == SDL.SDL_EventType.SDL_KEYDOWN)
                {
                    if (e.button.button == 82)
                        textRect.Height++;
                    else if (e.button.button == 81)
                        textRect.Height--;
                    else if (e.button.button == 79)
                        textRect.Width++;
                    else if (e.button.button == 80)
                        textRect.Width--;
                    else
                        Quit();
                }
                if (e.type == SDL.SDL_EventType.SDL_MOUSEBUTTONDOWN)
                    Quit();

            }
        }

        public override void Update(GameTime gameTime)
        {

            DoEvent();
            rect.X = c;
            //textRect.Height = c;
            rotation.X = (int)(Math.Cos(((double)c % 360) / 360 * Math.PI * 2) * 100 + 100);
            rotation.Y = (int)(Math.Sin(((double)c % 360) / 360 * Math.PI * 2) * 100 + 100);
            allInts[c % allInts.Length] = (int)(1.0 / gameTime.SinceLastUpdate.TotalSeconds);
            //double num = gameTime.SinceLastUpdate.TotalMilliseconds;
            textSurface.Dispose();
            textTexture.Dispose();
            textSurface = SDLSurface.FromPointer(SDL_ttf.TTF_RenderText_Solid(ptr, allInts.Average().ToString("0000") , new SDL.SDL_Color() { r = 255, g = 255, b = 255}));
            textTexture = SDLTexture.CreateFrom(renderer, textSurface);
            c++;
        }

        public override void Draw(GameTime gameTime)
        {
            renderer.Clear();
            SDLRectangle testRect = new SDLRectangle(0, 0, 100, 100);
            renderer.Copy(tester, ref testRect);
            renderer.Copy(texture, ref rect);
            renderer.Copy(textTexture, ref textRect);
            renderer.Copy(texture, ref rotation);
            renderer.DrawPoint(c, c, new SDLColor(255, 0, 0));
            renderer.Present();
            double delayNum = ((1.0 / 60.0) * 1000 - gameTime.SinceLastUpdate.TotalMilliseconds);
            SDL.SDL_Delay(delayNum < 0 ? 0 : (uint)delayNum);
        }
    }*/
}

