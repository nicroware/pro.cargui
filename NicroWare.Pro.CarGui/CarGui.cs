﻿using System;
using System.Runtime.InteropServices;
using System.IO;
using System.Collections.Generic;
using NicroWare.Lib.Sensors;
using NicroWare.Lib.Networking.Manager;
using NicroWare.Lib.SdlEngine;
using NicroWare.Lib.SdlGui;
using NicroWare.Lib.SdlEngine.Events;
using NicroWare.Lib.Networking.Logger;

namespace NicroWare.Pro.CarGui
{
    public partial class CarGui : SDLRenderWindow
    {
        SDLRenderer renderer;

        int maxSpeed = 300;
            
        SDLFont digiFont;

        LogManager mainLogger;

        KeyboardState curKey;
        KeyboardState prevKey;
        MouseState prevMouse;
        MouseState curMouse;

        PageScroller scroller;

        public CarGui()
        {
            MouseData.LastPos = new SDLPoint(-1, -1);
            mainLogger = new LogManager();
            //mainLogger.OnVerbose += MainLogger_OnWarning;
            mainLogger.OnWarning += MainLogger_OnWarning;
        }

        void MainLogger_OnWarning (object sender, LogWriteEventArgs e)
        {
            listView_event.Items.Add(string.Format("({0})[{1}]\t{2}", e.Entry.Time.ToString("HH:mm:ss"), e.Entry.Sender, e.Entry.Value));
            if (e.Entry is CanEntry && e.Entry.Sender == "CANMSG")
            {
                textBox_infoBlock.Text = e.Entry.Value;
                switch ((e.Entry as CanEntry).CanMessage.SensorID)
                {
                    case 100:
                        image_warningBox.ImageColor = SDLColor.Red;
                        image_pedalWarning.ImageColor = SDLColor.Red;
                        break;
                    case 101:
                        image_warningBox.ImageColor = SDLColor.Red;
                        image_startupWarning.ImageColor = SDLColor.Red;
                        image_engineWarning.ImageColor = SDLColor.Red;
                        break;
                }
            }
        }


        public override void Initialize()
        {

            renderer = SDLRenderer.Create(this);
            Event.Updater += EventUpdater;
            InitializeTan();

            FileInfo fontFile = new FileInfo("Content/SFDigital.ttf");
            //FileInfo font2File = new FileInfo("Content/unispace.ttf");
            FileInfo font2File = new FileInfo("Content/MonospaceTypewriter.ttf");

            digiFont = SDLFont.LoadFont(fontFile.FullName, 150);
            Global.Font = SDLFont.LoadFont(font2File.FullName, 50);

            scroller = new PageScroller() { Location = new SDLPoint(0,0), Size = new SDLPoint(Size.X, Size.Y)};
            InitializePage0();
            InitializePage1();
            InitializePage2();
            InitializePage3();

            DataReciver.Initialize(mainLogger);

            scroller.CurrentPage = 1;

            base.Initialize();
        }

        public void InitializeTan()
        {
            Global.TanPos = new int[451, 451];
            for (int y = 0; y < 451; y++)
            {
                for (int x = 0; x < 451; x++)
                {
                    Global.TanPos[x, y] = (int)(Math.Atan2(y - 225, x - 225) / (Math.PI * 2) * 360 + 0.5 );
                }
            }
        }

        public override void LoadContent()
        {
            base.LoadContent();
        }

        public override void UnloadContent()
        {
            
        }

        public void EventUpdater()
        {
            InputEvent? evn;
            MouseState current = Mouse.GetState();
            while ((evn = Input.GetEvent()) != null)
            {
                InputEvent ev = evn.Value;
                if (ev.type == 3)
                {
                    Global.SideScroll = true;
                    if (ev.code == 0)
                    {
                        Mouse.SetMousePos((int)(ev.value / 4096.0 * Size.X), current.Location.Y);
                        Mouse.AddKey(MouseButtons.Left);
                        current = Mouse.GetState();
                    }
                    else if (ev.code == 1)
                    {
                        Mouse.SetMousePos(current.Location.X, (int)(ev.value / 4096.0 * Size.Y));
                        Mouse.AddKey(MouseButtons.Left);
                        current = Mouse.GetState();
                    }
                }
                if (ev.type == 1)
                {
                    if (ev.code == 272 && ev.value == 0)
                    {
                        Mouse.RemoveKey(MouseButtons.Left);
                    }
                }
            }
        }

        public void DoEvent()
        {
            // Stores current in prev for being able to detect push down and push up
            prevKey = curKey;
            prevMouse = curMouse;

            curKey = Keyboard.GetState();
            curMouse = Mouse.GetState();

            //Unused
            MouseData.LastPos = new SDLPoint(-1, -1);

            //Test code to simulate speed and page switch
            if (curKey.IsKeyDown(Keys.UP))
                speedometer.Value += 1;
            else if (curKey.IsKeyDown(Keys.DOWN))
                speedometer.Value -= 1;
            if (curKey.IsKeyDown(Keys.LEFT) && !prevKey.IsKeyDown(Keys.LEFT))
                scroller.CurrentPage--;
            else if (curKey.IsKeyDown(Keys.RIGHT) && !prevKey.IsKeyDown(Keys.RIGHT))
                scroller.CurrentPage++;


            if (curMouse.IsKeyDown(MouseButtons.Left))
            {
                if (!prevMouse.IsKeyDown(MouseButtons.Left))
                {
                    Global.SideScroll = true;
                    MouseData.Pos = curMouse.Location;
                    MouseData.LastPos = curMouse.Location;
                    MouseData.LastDownPos = curMouse.Location;
                    Console.WriteLine("Mouse down at, X: " + MouseData.Pos.X + " Y: " + MouseData.Pos.Y);
                    MouseData.Down = true;
                }
                else
                {
                    MouseData.Pos = curMouse.Location;
                }
            }
            else if (!curMouse.IsKeyDown(MouseButtons.Left) && prevMouse.IsKeyDown(MouseButtons.Left))
            {
                if (Global.SideScroll)
                {
                    //CheckShiftPage();
                }
                MouseData.Down = false;
                //activePage.Location = new SDLPoint(0, 0);
            }
        }

        public void CheckShiftPage()
        {
            if (MouseData.Pos.X - MouseData.LastDownPos.X < -100)
                scroller.CurrentPage++;
            else if (MouseData.Pos.X - MouseData.LastDownPos.X > 100)
                scroller.CurrentPage--;
        }

        public void UpdateValues()
        {
            ushort[] sensors = SensorLookup.SensorIDs;
            DataWrapper? wrapper = null;

            //Advanced switch body, updates sensor values as it should
            SwitchBody<SensorLookup> switcher = SwitchBody<SensorLookup>.CreateSwitch()
                .Case(SensorLookup.GetByName(SensorLookup.SPEED), () => speedometer.Value = wrapper.Value.Value - SensorLookup.GetByName(SensorLookup.SPEED).LowLimit)
                .Case(SensorLookup.GetByName(SensorLookup.SOC), () => infometer_bat.Value = wrapper.Value.Value - SensorLookup.GetByName(SensorLookup.SOC).LowLimit)
                .Case(SensorLookup.GetByName(SensorLookup.VOLTAGE12), () => infometer_volt.Value = wrapper.Value.Value - SensorLookup.GetByName(SensorLookup.VOLTAGE12).LowLimit)
                .Case(SensorLookup.GetByName(SensorLookup.TEMPBAT), () => infometer_tempBat.Value = wrapper.Value.Value - SensorLookup.GetByName(SensorLookup.TEMPBAT).LowLimit)
                .Case(SensorLookup.GetByName(SensorLookup.TEMPCOOL), () => infometer_tempCool.Value = wrapper.Value.Value - SensorLookup.GetByName(SensorLookup.TEMPCOOL).LowLimit)
                .Case(SensorLookup.GetByName(SensorLookup.SPEEDPEDDAL), () => speedPeddalMeter.Value = wrapper.Value.Value - SensorLookup.GetByName(SensorLookup.SPEEDPEDDAL).LowLimit)
                .Case(SensorLookup.GetByName(SensorLookup.BREAKPEDDAL), () => breakPeddalMeter.Value = wrapper.Value.Value - SensorLookup.GetByName(SensorLookup.BREAKPEDDAL).LowLimit)
                .Case(SensorLookup.GetByName(SensorLookup.DRVWHEEL), () => wheelTurnMeter.Value = wrapper.Value.Value - SensorLookup.GetByName(SensorLookup.DRVWHEEL).LowLimit)
                .Case(SensorLookup.GetByName(SensorLookup.SUSPENSION), () => suspensionMeter.Value = wrapper.Value.Value - SensorLookup.GetByName(SensorLookup.SUSPENSION).LowLimit)
                .Case(SensorLookup.GetByName(SensorLookup.CURRENT), () => currentBarMeter.Value = wrapper.Value.Value - SensorLookup.GetByName(SensorLookup.CURRENT).LowLimit)
                ;
            
            for (ushort i = 0; i < sensors.Length; i++)
            {
                //Get lastes wrapper
                wrapper = DataReciver.GetData(i);

                if (wrapper != null)
                {
                    switcher.Switch(SensorLookup.GetById(i)).Run();
                }
            }
        }

        public override void Update(GameTime gameTime)
        {
            UpdateValues();
            DoEvent();

            scroller.Update(gameTime);

            if (!DataReciver.DriveToSide)
            {
                label_speed.TextColor = SDLColor.White;
                label_speed.Text = (speedometer.Persent * maxSpeed).ToString("000"); 
                label_speed2.Text = "";

            }
            else
            {
                label_speed.TextColor = SDLColor.Green;
                label_speed2.TextColor = SDLColor.Green;
                label_speed.Text = "DRIVE";
                label_speed2.Text = "TO SIDE";

                if (DataReciver.MessageReceived + TimeSpan.FromSeconds(5) < DateTime.Now)
                {
                    DataReciver.DriveToSide = false;
                }
            }
            //Fancy sinus wave, for sliders
            /*double part = Math.PI * 2 / allScolls.Length;
            for (int i = 0; i < allScolls.Length; i++)
            {
                allScolls[i].Value = (Math.Sin(sinCounter + part * i) + 1) / 2;
            }
            sinCounter += 0.005;*/

        }

        public override void Draw(GameTime gameTime)
        {
            renderer.Clear();

            scroller.Draw(renderer, gameTime);

            renderer.Present();
        }
    }
}

