﻿using System;
using System.IO;
using NicroWare.Lib.SdlEngine;
using NicroWare.Lib.SdlGui;
using NicroWare.Lib.Networking.Manager;
using NicroWare.Lib.Sensors;


namespace NicroWare.Pro.CarGui
{
	public partial class CarGui
	{
        ListView listView_event;

        Image image_bat;
        Image image_bat12;
        Image image_batTemp;
        Image image_coolTemp;

        Image image_pedalWarning;
        Image image_engineWarning;
        Image image_warningBox;
        Image image_startupWarning;
        Image image_breakWarning;
        Image image_temperature;


        Speedometer speedometer;

        InfoMeter infometer_bat;
        InfoMeter infometer_volt;
        InfoMeter infometer_tempBat;
        InfoMeter infometer_tempCool;

        BarMeter speedPeddalMeter;
        BarMeter breakPeddalMeter;
        BarMeter wheelTurnMeter;
        BarMeter suspensionMeter;
        BarMeter currentBarMeter;

        Label label_speed;
        Label label_speed2;

        TextBlock textBox_infoBlock;

        public void InitializePage0()
        {
            GuiPage page0 = new GuiPage(renderer);
            listView_event = new ListView()
            {
                FontSize = 17,
                Location = new SDLPoint(0, 0),
                Size = new SDLPoint(800, 380),
            };
            /*eventView.Items.Add("ERROR1");
            eventView.Items.Add("ERROR2");
            eventView.Items.Add("ERROR3");
            eventView.Items.Add("ERROR4");
            eventView.Items.Add("ERROR5");
            eventView.Items.Add("ERROR6");
            eventView.Items.Add("ERROR7");
            eventView.Items.Add("ERROR8");
            eventView.Items.Add("ERROR9");
            eventView.Items.Add("ERROR10 ERROR10 ERROR10");
            eventView.Items.Add("ERROR11");
            eventView.Items.Add("ERROR12");
            eventView.Items.Add("ERROR13");
            eventView.Items.Add("ERROR14");
            eventView.Items.Add("ERROR15");
            eventView.Items.Add("ERROR16");
            eventView.Items.Add("ERROR17");
            eventView.Items.Add("ERROR18");
            eventView.Items.Add("ERROR19");
            eventView.Items.Add("ERROR20");
            eventView.Items.Add("ERROR21");
            eventView.Items.Add("ERROR22");
            eventView.Items.Add("ERROR23");
            eventView.Items.Add("ERROR24");
            eventView.Items.Add("ERROR25");
            eventView.Items.Add("ERROR26");
            eventView.Items.Add("ERROR27");
            eventView.Items.Add("ERROR28");
            eventView.Items.Add("ERROR29");
            eventView.Items.Add("ERROR30");
            eventView.Items.Add("ERROR31");
            eventView.Items.Add("ERROR32");
            eventView.Items.Add("ERROR33");
            eventView.Items.Add("ERROR34");
            eventView.Items.Add("ERROR35");
            eventView.Items.Add("ERROR36");
            eventView.Items.Add("ERROR37");
            eventView.Items.Add("ERROR38");
            eventView.Items.Add("ERROR1");
            eventView.Items.Add("ERROR2");
            eventView.Items.Add("ERROR3");
            eventView.Items.Add("ERROR4");
            eventView.Items.Add("ERROR5");
            eventView.Items.Add("ERROR6");
            eventView.Items.Add("ERROR7");
            eventView.Items.Add("ERROR8");
            eventView.Items.Add("ERROR9");
            eventView.Items.Add("ERROR10 ERROR10 ERROR10");
            eventView.Items.Add("ERROR11");
            eventView.Items.Add("ERROR12");
            eventView.Items.Add("ERROR13");
            eventView.Items.Add("ERROR14");
            eventView.Items.Add("ERROR15");
            eventView.Items.Add("ERROR16");
            eventView.Items.Add("ERROR17");
            eventView.Items.Add("ERROR18");
            eventView.Items.Add("ERROR19");
            eventView.Items.Add("ERROR20");
            eventView.Items.Add("ERROR21");
            eventView.Items.Add("ERROR22");
            eventView.Items.Add("ERROR23");
            eventView.Items.Add("ERROR24");
            eventView.Items.Add("ERROR25");
            eventView.Items.Add("ERROR26");
            eventView.Items.Add("ERROR27");
            eventView.Items.Add("ERROR28");
            eventView.Items.Add("ERROR29");
            eventView.Items.Add("ERROR30");
            eventView.Items.Add("ERROR31");
            eventView.Items.Add("ERROR32");
            eventView.Items.Add("ERROR33");
            eventView.Items.Add("ERROR34");
            eventView.Items.Add("ERROR35");
            eventView.Items.Add("ERROR36");
            eventView.Items.Add("ERROR37");
            eventView.Items.Add("ERROR38");

*/

            Button button_clear = new Button()
            {
                Location = new SDLPoint(15,410),

                Text = "Clear",
                Size = new SDLPoint(150, 50),
                DynamicTextSize = true,
                TextAlign = Align.Center,

            };
            button_clear.Click += (object sender, EventArgs e) => 
            {
                listView_event.Items.Clear(); 
                listView_event.Scroll = 0; 
            };

            Button button_clearLight = new Button()
            {
                Location = new SDLPoint(200,410),

                Text = "Clear Light",
                Size = new SDLPoint(150, 50),
                DynamicTextSize = true,
                TextAlign = Align.Center,

            };
            button_clearLight.Click += (object sender, EventArgs e) => 
            {
                image_pedalWarning.ImageColor = new SDLColor(50, 50, 50);
                image_engineWarning.ImageColor = new SDLColor(50, 50, 50);
                image_warningBox.ImageColor = new SDLColor(50, 50, 50);
                image_startupWarning.ImageColor = new SDLColor(50, 50, 50);
                image_breakWarning.ImageColor = new SDLColor(50, 50, 50);
                image_temperature.ImageColor = new SDLColor(50, 50, 50);
                textBox_infoBlock.Text = "\nNo Error!";
            };


            page0.Elements.Add(listView_event);
            page0.Elements.Add(button_clear);
            page0.Elements.Add(button_clearLight);
            scroller.AllPages.Add(page0);
            page0.Initialize(renderer);

        }

        public void InitializePage1()
        {
            speedometer = new Speedometer() 
            { 
                Location = new SDLPoint(175, 15), 
                MaxValue = SensorLookup.GetByName(SensorLookup.SPEED).SpanLimit 
            };

            infometer_bat = new InfoMeter() 
            { 
                Location = new SDLPoint(550, 35), 
                MaxValue = SensorLookup.GetByName(SensorLookup.SOC).SpanLimit 
            };

            infometer_volt = new InfoMeter() 
            { 
                Location = new SDLPoint(90, 35), 
                Flip = RendererFlip.Horisontal, 
                MaxValue = SensorLookup.GetByName(SensorLookup.VOLTAGE12).SpanLimit 
            };

            infometer_tempBat = new InfoMeter() 
            { 
                Location = new SDLPoint(90, 260), 
                Flip = RendererFlip.Both, 
                MaxValue = SensorLookup.GetByName(SensorLookup.TEMPBAT).SpanLimit 
            };

            infometer_tempCool = new InfoMeter() 
            { 
                Location = new SDLPoint(550, 260), 
                Flip =  RendererFlip.Vertical, 
                MaxValue = SensorLookup.GetByName(SensorLookup.TEMPCOOL).SpanLimit
            };

            image_bat = Image.LoadFromFile(renderer, "Content/Battery.bmp").Initialize(x =>
            {
                x.Location = new SDLPoint(720,100);
                x.Size = new SDLPoint(32,32);
                x.ImageColor = new SDLColor(255,255,255);
            });

            image_bat12 = Image.LoadFromFile(renderer, "Content/Battery12.bmp").Initialize(x =>
            {
                x.Location = new SDLPoint(48,100);
                x.Size = new SDLPoint(32,32);
                x.ImageColor = new SDLColor(255,255,255);
            });

            image_coolTemp = Image.LoadFromFile(renderer, "Content/CoolantTemperature.bmp").Initialize(x =>
            {
                x.Location = new SDLPoint(50,330);
                x.Size = new SDLPoint(32,32);
                x.ImageColor = new SDLColor(255,255,255);
            });

            image_coolTemp = Image.LoadFromFile(renderer, "Content/CoolantTemperature.bmp").Initialize(x =>
            {
                x.Location = new SDLPoint(50,330);
                x.Size = new SDLPoint(32,32);
                x.ImageColor = new SDLColor(255,255,255);
            });

            image_batTemp = Image.LoadFromFile(renderer, "Content/BatteryTemperature.bmp").Initialize(x =>
            {
                x.Location = new SDLPoint(710,330);
                x.Size = new SDLPoint(32,32);
                x.ImageColor = new SDLColor(255,255,255);
            });

            image_warningBox = Image.LoadFromFile(renderer, "Content/Warning.bmp").Initialize(x =>
            {
                x.Location = new SDLPoint(260,425);
                x.Size = new SDLPoint(32,32);
                x.ImageColor = new SDLColor(50,50,50);
            });

            image_startupWarning = Image.LoadFromFile(renderer, "Content/StartupWarning.bmp").Initialize(x =>
            {
                x.Location = new SDLPoint(300,425);
                x.Size = new SDLPoint(32,32);
                x.ImageColor = new SDLColor(50,50,50);
            });

            image_breakWarning = Image.LoadFromFile(renderer, "Content/BreakWarning.bmp").Initialize(x =>
            {
                x.Location = new SDLPoint(340,425);
                x.Size = new SDLPoint(32,32);
                x.ImageColor = new SDLColor(50,50,50);
            });

            image_temperature = Image.LoadFromFile(renderer, "Content/Temperature.bmp").Initialize(x =>
            {
                x.Location = new SDLPoint(380,425);
                x.Size = new SDLPoint(32,32);
                x.ImageColor = new SDLColor(50,50,50);
            });

            image_pedalWarning = Image.LoadFromFile(renderer, "Content/PedalWarning.bmp").Initialize(x =>
            {
                x.Location = new SDLPoint(420,425);
                x.Size = new SDLPoint(32,32);
                x.ImageColor = new SDLColor(50,50,50);
            });

            image_engineWarning = Image.LoadFromFile(renderer, "Content/EngineWarning.bmp").Initialize(x =>
            {
                x.Location = new SDLPoint(460,425);
                x.Size = new SDLPoint(32,32);
                x.ImageColor = new SDLColor(50,50,50);
            });

            label_speed = new Label()
            {
                Location = new SDLPoint(327,200 + 15),
                Size = new SDLPoint(150, 60),
                Text = "888",
                Font = digiFont,
                DynamicTextSize = false,
            };
            label_speed2 = new Label()
            {
                Location = new SDLPoint(327,200 + 15 + 60),
                Size = new SDLPoint(150, 60),
                Text = "",
                Font = digiFont,
                DynamicTextSize = false,
            };

            Label label_batMax = new Label()
            {
                Location = new SDLPoint(700, 25),
                Size = new SDLPoint(18 * 3, 25),
                Text = SensorLookup.GetByName(SensorLookup.SOC).MaxValue.ToString(),
                Font = digiFont,
            };
            Label label_batMin = new Label()
            {
                Location = new SDLPoint(700, 205),
                Size = new SDLPoint(18 * 2, 25),
                Text = " " + SensorLookup.GetByName(SensorLookup.SOC).MinValue.ToString(),
                Font = digiFont,
            };
            Label label_voltageMax = new Label()
            {
                Location = new SDLPoint(40, 25),
                Size = new SDLPoint(18 * 5, 25),
                Text = " " + SensorLookup.GetByName(SensorLookup.VOLTAGE12).MaxValue.ToString(),
                Font = digiFont,
            };
            Label label_voltageMin = new Label()
            {
                Location = new SDLPoint(40, 205),
                Size = new SDLPoint(18 * 3, 25),
                Text = " " + SensorLookup.GetByName(SensorLookup.VOLTAGE12).MinValue.ToString(),
                Font = digiFont,
            };
            Label label_tempCoolMax = new Label()
            {
                Location = new SDLPoint(40, 250),
                Size = new SDLPoint(18 * 5, 25),
                Text = " " + SensorLookup.GetByName(SensorLookup.TEMPCOOL).MaxValue.ToString(),
                Font = digiFont,
            };
            Label label_tempCoolMin = new Label()
            {
                Location = new SDLPoint(40, 430),
                Size = new SDLPoint(18 * 3, 25),
                Text = " " + SensorLookup.GetByName(SensorLookup.TEMPCOOL).MinValue.ToString(),
                Font = digiFont,
            };
            Label label_tempBatMax = new Label()
            {
                Location = new SDLPoint(700, 250),
                Size = new SDLPoint(18 * 3, 25),
                Text = " " + SensorLookup.GetByName(SensorLookup.TEMPBAT).MaxValue.ToString(),
                Font = digiFont,
            };
            Label label_tempBatMin = new Label()
            {
                Location = new SDLPoint(700, 430),
                Size = new SDLPoint(18 * 2, 25),
                Text = " " + SensorLookup.GetByName(SensorLookup.TEMPBAT).MinValue.ToString(),
                Font = digiFont,
            };

            textBox_infoBlock = new TextBlock()
            {
                Location = new SDLPoint(220, 350),
                Text = "\nNo error!",
                FontSize = 20,
                Size = new SDLPoint(360, 20),
                TextAlign = Align.Center,
            };
            Button button1 = new Button() { Text = "Next", Location = new SDLPoint(570, 380)};;
            button1.Click += (object sender, EventArgs e) => scroller.CurrentPage++;
            GuiPage page1 = new GuiPage(renderer);

            //Add elements
            page1.Elements.Add(infometer_bat);
            page1.Elements.Add(infometer_volt);
            page1.Elements.Add(infometer_tempBat);
            page1.Elements.Add(infometer_tempCool);
            page1.Elements.Add(speedometer);
            //page1.Elements.Add(wheelTurn);
            page1.Elements.Add(textBox_infoBlock);

            //Labels
            page1.Elements.Add(label_speed);
            page1.Elements.Add(label_speed2);
            page1.Elements.Add(label_batMax);
            page1.Elements.Add(label_batMin);
            page1.Elements.Add(label_voltageMax);
            page1.Elements.Add(label_voltageMin);
            page1.Elements.Add(label_tempBatMax);
            page1.Elements.Add(label_tempBatMin);
            page1.Elements.Add(label_tempCoolMax);
            page1.Elements.Add(label_tempCoolMin);

            //Images
            page1.Elements.Add(image_bat);
            page1.Elements.Add(image_bat12);
            page1.Elements.Add(image_coolTemp);
            page1.Elements.Add(image_batTemp);
            page1.Elements.Add(image_warningBox);
            page1.Elements.Add(image_startupWarning);
            page1.Elements.Add(image_breakWarning);
            page1.Elements.Add(image_temperature);
            page1.Elements.Add(image_engineWarning);
            page1.Elements.Add(image_pedalWarning);

            //page1.Elements.Add(button1);

            //Add page
            scroller.AllPages.Add(page1);
            page1.Initialize(renderer);
        }

        public void InitializePage2()
        {
            GuiPage page2 = new GuiPage(renderer);

            Button button2 = new Button() { Text = "Back", Location = new SDLPoint(30, 380) };
            Button exit = new Button() { Text = "Exit", Location = new SDLPoint(570, 380) };
            ScrollBar scrollBar = new ScrollBar() { Location = new SDLPoint(20, 20) };
            ScrollBar scrollBar1 = new ScrollBar() { Location = new SDLPoint(120, 20) };
            ScrollBar scrollBar2 = new ScrollBar() { Location = new SDLPoint(220, 20) };
            ScrollBar scrollBar3 = new ScrollBar() { Location = new SDLPoint(320, 20) };
            ScrollBar scrollBar4 = new ScrollBar() { Location = new SDLPoint(420, 20) };
            ScrollBar scrollBar5 = new ScrollBar() { Location = new SDLPoint(520, 20) };
            ScrollBar scrollBar6 = new ScrollBar() { Location = new SDLPoint(620, 20) };
            ScrollBar scrollBar7 = new ScrollBar() { Location = new SDLPoint(720, 20) };
            button2.Click += (object sender, EventArgs e) =>
            {
                scroller.CurrentPage--;
                /* This is test code
                image_temperature.ImageColor = SDLColor.Red;
                image_bat.ImageColor = SDLColor.Red;
                image_warningBox.ImageColor = SDLColor.Red;
                image_startupWarning.ImageColor = SDLColor.Red;
                image_breakWarning.ImageColor = SDLColor.Red;

                textBox_infoBlock.Text = "This display shows info\nIt looks like there is\nsome warnings"; */
            };
            exit.Click += (object sender, EventArgs e) => this.Quit();
            page2.Elements.Add(button2);
            //page2.Elements.Add(exit);
            page2.Elements.Add(scrollBar);
            page2.Elements.Add(scrollBar1);
            page2.Elements.Add(scrollBar2);
            page2.Elements.Add(scrollBar3);
            page2.Elements.Add(scrollBar4);
            page2.Elements.Add(scrollBar5);
            page2.Elements.Add(scrollBar6);
            page2.Elements.Add(scrollBar7);
            //allScolls = new ScrollBar[] { scrollBar, scrollBar1, scrollBar2, scrollBar3, scrollBar4, scrollBar5, scrollBar6, scrollBar7 };
            scroller.AllPages.Add(page2);
            page2.Initialize(renderer);
        }

        public void InitializePage3()
        {
            //            FileInfo labelFont = new FileInfo("Content/Open24.ttf");
            //            SDLFont LabelFont = SDLFont.LoadFont(labelFont.FullName, 150);

            speedPeddalMeter = new BarMeter() 
            { 
                Location = new SDLPoint(680, 120),
                MaxValue = SensorLookup.GetByName(SensorLookup.SPEEDPEDDAL).SpanLimit
            };
            breakPeddalMeter = new BarMeter() 
            { 
                Location = new SDLPoint(70, 120),
                MaxValue = SensorLookup.GetByName(SensorLookup.BREAKPEDDAL).SpanLimit
            };
            wheelTurnMeter = new BarMeter() 
            { 
                Location = new SDLPoint(380, 270),
                MaxValue = SensorLookup.GetByName(SensorLookup.DRVWHEEL).SpanLimit
            };
            suspensionMeter = new BarMeter() 
            { 
                Location = new SDLPoint(380, 290),
                MaxValue = SensorLookup.GetByName(SensorLookup.SUSPENSION).SpanLimit
            };
            currentBarMeter = new BarMeter() 
            { 
                Location = new SDLPoint(380, 290),
                MaxValue = SensorLookup.GetByName(SensorLookup.CURRENT).SpanLimit
            };

            Label hundredLabel = new Label()
            {
                Location = new SDLPoint(0, 10),
                Size = new SDLPoint(Size.X, 30),
                Text = "Calibration Options",
                TextAlign = Align.Center,
            };

            Button steering = new Button() { Text = "Steering", Location = new SDLPoint(100, 100), DynamicTextSize = true };
            steering.Click += (object sender, EventArgs e) => {
                scroller.ActivePage = new GuiPage(renderer);
                Global.SideScroll = false;
                /* Title label */
                scroller.ActivePage.Elements.Add( new Label() {
                    Location = new SDLPoint(0, 10), 
                    Text = "Steering calibration", // 20
                    Size = new SDLPoint(0, 30),
                    DynamicTextSize = true
                } );


                /*activePage.Elements.Add( new Label() {
                    Location = new SDLPoint(10, 50), 
                    Text = "For Left and Right,", // 83
                    Size = new SDLPoint(0, 20),
                    DynamicTextSize = true
                } );*/

                scroller.ActivePage.Elements.Add( new TextBlock() {
                    Location = new SDLPoint(10, 50),
                    Size = new SDLPoint(800, 20),
                    FontSize = 20,
                    Text = "For Left and Right, turn the steering wheel\ncompletely to their respective positions."
                } );

                /* Set steering left max */
                scroller.ActivePage.Elements.Add( new Label() {
                    Location = new SDLPoint(100-(54/2), 150), 
                    Text = "LEFT", // 20
                    Size = new SDLPoint(0, 30),
                    DynamicTextSize = true
                } );
                Button stLeftButton = new Button() {
                    Location = new SDLPoint(5, 180),
                    Text = "Calibrate Left",
                    DynamicTextSize = true,
                };
                stLeftButton.Click += (object sender2, EventArgs e2) => {
                    DataReciver.canClient.Send(new DataWrapper() { SensorID = 0, Value = 4}.GetBytes(true));
                    Console.WriteLine("Steering Left OK");
                    stLeftButton.Text = "Left OK";
                    stLeftButton.TextColor = SDLColor.Green;
                    stLeftButton.FrameColor = SDLColor.Green;
                };

                scroller.ActivePage.Elements.Add( new Label() {
                    Location = new SDLPoint(0, 150), 
                    Text = "CENTER", // 20
                    Size = new SDLPoint(Size.X, 30),
                    TextAlign = Align.Center
                } );
                Button stCenterButton = new Button() {
                    Location = new SDLPoint(400-(197/2), 180),
                    Text = "Calibrate Center",
                    DynamicTextSize = true
                };
                stCenterButton.Click += (object sender2, EventArgs e2) => {
                    DataReciver.canClient.Send(new DataWrapper() { SensorID = 0, Value = 5}.GetBytes(true));
                    Console.WriteLine("Steering Center OK");
                    stCenterButton.Text = "Center OK";
                    stCenterButton.TextColor = SDLColor.Green;
                    stCenterButton.FrameColor = SDLColor.Green;
                    //DataReciver.canClient.Send();
                };

                scroller.ActivePage.Elements.Add( new Label() {
                    Location = new SDLPoint(700-(90/2), 150), 
                    Text = "RIGHT", // 20
                    Size = new SDLPoint(0, 30),
                    DynamicTextSize = true
                } );
                Button stRightButton = new Button() {
                    Location = new SDLPoint(700-(210/2), 180),
                    Text = "Calibrate Right",
                    DynamicTextSize = true
                };
                stRightButton.Click += (object sender2, EventArgs e2) => {
                    DataReciver.canClient.Send(new DataWrapper() { SensorID = 0, Value = 6}.GetBytes(true));
                    Console.WriteLine("Steering Right OK");
                    stRightButton.Text = "Right OK";
                    stRightButton.TextColor = SDLColor.Green;
                    stRightButton.FrameColor = SDLColor.Green;
                };

                Button backButton = new Button() { Location = new SDLPoint(10, 400), Text = "Back", DynamicTextSize = true };
                backButton.Click += (object sender2, EventArgs e2) => scroller.CurrentPage = scroller.CurrentPage;

                scroller.ActivePage.Elements.Add(wheelTurnMeter);

                scroller.ActivePage.Elements.Add(stLeftButton);
                scroller.ActivePage.Elements.Add(stCenterButton);
                scroller.ActivePage.Elements.Add(stRightButton);
                scroller.ActivePage.Elements.Add(backButton);
                scroller.ActivePage.Initialize(renderer);
            };

            Button pedals = new Button() { Text = "Pedals", Location = new SDLPoint(100, 250), DynamicTextSize = true };
            pedals.Click += (object sender, EventArgs e) => {
                scroller.ActivePage = new GuiPage(renderer);

                scroller.ActivePage.Elements.Add( new Label() {
                    Location = new SDLPoint(0, 10),
                    Text = "Pedal Calibration", // 17
                    Size = new SDLPoint(Size.X, 30),
                    TextAlign = Align.Center
                } );

                /* Set torque and brake pedal min button and label */
                scroller.ActivePage.Elements.Add( new Label() {
                    Location = new SDLPoint(0, 50),
                    Text = "Release both pedals", // 28
                    Size = new SDLPoint(Size.X, 20),
                    TextAlign = Align.Center
                } );
                Button setMinButton = new Button() {
                    Location = new SDLPoint(300, 80), 
                    Text = "Set Pedal Min", 
                    DynamicTextSize = true,
                };
                setMinButton.Click += (object sender2, EventArgs e2) => {
                    DataReciver.canClient.Send(new DataWrapper() { SensorID = 0, Value = 0}.GetBytes(true));
                    Console.WriteLine("Pedal Min OK");
                    setMinButton.Text = "Pedal Min OK";
                    setMinButton.TextColor = SDLColor.Green;
                    setMinButton.FrameColor = SDLColor.Green;
                };

                /* Set torque max button and label */
                scroller.ActivePage.Elements.Add( new Label() { 
                    Location = new SDLPoint(0, 180), // (20 * 0.6 * 32) => (Size y * 0.6 * Text width)
                    Text = "Press torque pedal in completely", // 32
                    Size = new SDLPoint(Size.X, 20),
                    TextAlign = Align.Center,
                } );
                Button tqMaxButton = new Button() { 
                    Location = new SDLPoint(300, 210), 
                    Text = "Set Torque Max", 
                    DynamicTextSize = true 
                };
                tqMaxButton.Click += (object sender2, EventArgs e2) => {
                    DataReciver.canClient.Send(new DataWrapper() { SensorID = 0, Value = 1}.GetBytes(true));
                    Console.WriteLine("Torque Max OK");
                    tqMaxButton.Text = "Torque Max OK";
                    tqMaxButton.TextColor = SDLColor.Green;
                    tqMaxButton.FrameColor = SDLColor.Green;
                };

                /* Set brake pedal max button and label */
                scroller.ActivePage.Elements.Add( new Label() {
                    Location = new SDLPoint(0, 300),
                    Text = "Press brake pedal in completely",
                    Size = new SDLPoint(Size.X, 20),
                    TextAlign = Align.Center,
                } );
                Button brkMaxButton = new Button() {
                    Location = new SDLPoint(300, 330), 
                    Text = "Set Brake Max", 
                    DynamicTextSize = true 
                };
                brkMaxButton.Click += (object sender2, EventArgs e2) => {
                    DataReciver.canClient.Send(new DataWrapper() { SensorID = 0, Value = 2}.GetBytes(true));
                    Console.WriteLine("Brake Max OK");
                    brkMaxButton.Text = "Brake Max OK";
                    brkMaxButton.TextColor = SDLColor.Green;
                    brkMaxButton.FrameColor = SDLColor.Green;
                };

                Button backButton = new Button() { Location = new SDLPoint(10, 400), Text = "Back", DynamicTextSize = true };
                backButton.Click += (object sender2, EventArgs e2) => scroller.CurrentPage = scroller.CurrentPage;

                scroller.ActivePage.Elements.Add(speedPeddalMeter);
                scroller.ActivePage.Elements.Add(breakPeddalMeter);

                scroller.ActivePage.Elements.Add(backButton);
                scroller.ActivePage.Elements.Add(tqMaxButton);
                scroller.ActivePage.Elements.Add(brkMaxButton);
                scroller.ActivePage.Elements.Add(setMinButton);
                scroller.ActivePage.Initialize(renderer);

            };

            Button power = new Button() { Text = "Current", Location = new SDLPoint(450, 100), DynamicTextSize = true };
            power.Click += (object sender, EventArgs e) => {
                scroller.ActivePage = new GuiPage(renderer);

                /* Title Label */
                scroller.ActivePage.Elements.Add( new Label() {
                    Location = new SDLPoint(0, 10),
                    Size = new SDLPoint(Size.X, 30),
                    Text = "Current Calibration", // 19
                    TextAlign = Align.Center,
                } );

                scroller.ActivePage.Elements.Add( new TextBlock() {
                    Location = new SDLPoint(10, 50),
                    Size = new SDLPoint(800, 20),
                    FontSize = 20,
                    Text = "Before setting current zero level make sure that\nno other device or sensor is pulling any current.",
                } );

                Button setCurrentZero = new Button() {
                    Location = new SDLPoint(300, 200),
                    Text = "Set Current Zero",
                    DynamicTextSize = true
                };
                setCurrentZero.Click += (object sender2, EventArgs e2) => {
                    DataReciver.canClient.Send(new DataWrapper() { SensorID = 0, Value = 9}.GetBytes(true));
                    Console.WriteLine("Current Zero OK");
                    setCurrentZero.Text = "Current OK";
                    setCurrentZero.TextColor = SDLColor.Green;
                    setCurrentZero.FrameColor = SDLColor.Green;
                };

                Button backButton = new Button() { Location = new SDLPoint(10, 400), Text = "Back", DynamicTextSize = true };
                backButton.Click += (object sender2, EventArgs e2) => scroller.CurrentPage = scroller.CurrentPage;

                scroller.ActivePage.Elements.Add(currentBarMeter);

                scroller.ActivePage.Elements.Add(setCurrentZero);
                scroller.ActivePage.Elements.Add(backButton);

                scroller.ActivePage.Initialize(renderer);
            };

            Button suspension = new Button() { Text = "Suspension", Location = new SDLPoint(450, 250), DynamicTextSize = true };
            suspension.Click += (object sender, EventArgs e) => {
                scroller.ActivePage = new GuiPage(renderer);

                scroller.ActivePage.Elements.Add( new Label() {
                    Location = new SDLPoint(0, 10),
                    Text = "Suspension Calibration", // 22
                    Size = new SDLPoint(Size.X, 30),
                    TextAlign = Align.Center,
                } );

                /* Please make sure the active driver is
                 * seated before calibrating the suspension.
                */
                scroller.ActivePage.Elements.Add( new TextBlock() {
                    Location = new SDLPoint(10, 50),
                    Size = new SDLPoint(800, 20),
                    FontSize = 20,
                    Text = "Please make sure the active driver is\nseated before calibrating the suspension."
                } );

                Button suspBtn = new Button() {
                    Location = new SDLPoint(300, 200),
                    Text = "Set Suspension",
                    DynamicTextSize = true
                };
                suspBtn.Click += (object sender2, EventArgs e2) => {
                    DataReciver.canClient.Send(new DataWrapper() { SensorID = 0, Value = 8}.GetBytes(true));
                    suspBtn.Text = "Suspension OK";
                    Console.WriteLine("Suspension OK");
                    suspBtn.TextColor = SDLColor.Green;
                    suspBtn.FrameColor = SDLColor.Green;
                };

                Button backButton = new Button() { Location = new SDLPoint(10, 400), Text = "Back", DynamicTextSize = true };
                backButton.Click += (object sender2, EventArgs e2) => scroller.CurrentPage = scroller.CurrentPage;

                scroller.ActivePage.Elements.Add(suspensionMeter);

                scroller.ActivePage.Elements.Add(suspBtn);
                scroller.ActivePage.Elements.Add(backButton);

                scroller.ActivePage.Initialize(renderer);
            };

            Button backBtn = new Button() { Text = "Back", Location = new SDLPoint(30, 380) };
            backBtn.Click += (object sender, EventArgs e) => scroller.CurrentPage--;

            GuiPage calibrationPage = new GuiPage(renderer);
            calibrationPage.Elements.Add(hundredLabel);
            scroller.AllPages.Add(calibrationPage);
            calibrationPage.Elements.Add(pedals);
            calibrationPage.Elements.Add(steering);
            calibrationPage.Elements.Add(power);
            calibrationPage.Elements.Add(suspension);
            calibrationPage.Elements.Add(backBtn);

            calibrationPage.Initialize(renderer);
        }

	}
}

