﻿using System;
using NicroWare.Lib.Networking.Logger;
using NicroWare.Lib.Networking.Manager;
using System.Collections.Generic;
using NicroWare.Lib.Sensors;

namespace NicroWare.Pro.CarGui
{
    public class CanEntry : LogEntry
    {
        public DataWrapper CanMessage { get; set; }

        public override string Value
        {
            get
            {
                return this.ToString();
            }
            set
            {
                
            }
        }

        public CanError GetError()
        {
            return CanError.GetError(CanMessage.SensorID, CanMessage.Value);
        }

        public override string ToString()
        {
            return GetError().ToString();
        }
    }


}

