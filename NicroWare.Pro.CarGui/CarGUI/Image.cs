﻿using System;
using NicroWare.Lib.SdlEngine;

namespace NicroWare.Pro.SdlTest
{
    public class Image : GuiElement
    {
        public SDLTexture ImageTexture { get; set; }

        public override void Draw(Renderer renderer, GameTime gameTime)
        {
            renderer.Draw(ImageTexture, new SDLRectangle(0, 0, ImageTexture.Width, ImageTexture.Height));
        }
    }
}

